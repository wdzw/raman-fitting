# flake8: noqa
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 14 09:29:16 2021

@author: zmg
"""


def _testing():
    peak1, res1_peak_spec, res2_peak_spec = (
        modname_1,
        fitres_1,
        fitres_2,
    )
    peak1, res1_peak_spec = "1st_6peaks+Si", self._1st["1st_6peaks+Si"]
